
const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    first_name: { type: String },
    last_name: { type: String },
    username: { type: String },
    phone_number: { type: Number },
    email: { type: String },
    country_name: { type: String },
    city: { type: String },
    latitude: { type: String },
    longitude: { type: String },
    avatar: { type: String },
    password: { type: String },
    roles: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Role"
        }
    ]


    // user_name: { type: String },
    // title: { type: String },
    // product_name: { type: String },
    // product_amount_from: { type: String },
    // product_description: { type: String },
    // avatar: { type: String },
    // status: { type: String },
})

UserSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Users', UserSchema);

