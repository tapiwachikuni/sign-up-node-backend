module.exports = (app) => {
    const roles = require('../Controllers/role.controller.js');

    // Create a new Role
    app.post('/api/roles', roles.create);

    // Retrieve all Roles
    app.get('/api/roles', roles.findAll);

    // Retrieve a single Role with roleId
    app.get('/api/roles/:roleId', roles.findOne);

    // Update a Note with roleId
    app.put('/api/roles/:roleId', roles.update);

    // Delete a Note with roleId
    app.delete('/api/roles/:roleId', roles.delete);

}